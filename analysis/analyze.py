
class AnalyzeData(object):

    def __init__(self, neg_file, pos_file, column, threshold):
        neg = open(neg_file, "r").read().strip().splitlines()
        pos = open(pos_file, "r").read().strip().splitlines()
        self.fn, self.tp = self._count_threshold(pos, column, threshold)
        self.tn, self.fp = self._count_threshold(neg, column, threshold)

    def debug(self):
        print 'Results:'
        print '# FP: %d' % (self.fp,)
        print '# TP: %d' % (self.tp,)
        print '# FN: %d' % (self.fn,)
        print '# TN: %d' % (self.tn,)
        print '# '
        print '# Precision: %f' % (self.precision,)
        print '#    Recall: %f' % (self.recall,)
        print '# '
        print '#    F: %f' % (self.F,)

    def _count_threshold(self, lines, column_no, threshold):
        """
        Count all columns in a specific list of lines.
        Returns two counters: Amount below threshold, Amount above threshold
        """
        all_values = []
        for line in lines:
            # Parse line
            path1, path2, values = line.split("\t")

            # Get column value
            value = values.split("/")[column_no]
            if value == "NaN":
                value = 0.0
            else:
                value = float(value)
            all_values.append(value)

        count = len([x for x in all_values if x < threshold])
        return count, len(all_values) - count

    @property
    def precision(self):
        return float(self.tp) / (self.tp + self.fp)

    @property
    def recall(self):
        return float(self.tp) / (self.tp + self.fn)

    @property
    def F(self):
        return 2 * (self.precision * self.recall) / (self.precision + self.recall)


def create_output(neg_file, pos_file, output_file):
    output = open(output_file, "w")
    output.write("Threshold,PMI_F,PMI_Precision,PMI_Recall,TFIDF_F,TFIDF_Precision,TFIDF_Recall,DICE_F,DICE_Precision,DICE_Recall\n")

    thresholds = [float(x)/100 for x in range(100+1)]
    for threshold in thresholds:
        pmi_lin = AnalyzeData(neg_file, pos_file, 0, threshold)
        tfidf_cos = AnalyzeData(neg_file, pos_file, 1, threshold)
        dice_cover = AnalyzeData(neg_file, pos_file, 2, threshold)

        l = "%.2f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n" % (threshold, pmi_lin.F, pmi_lin.precision, pmi_lin.recall,
                    tfidf_cos.F, tfidf_cos.precision, tfidf_cos.recall,
                    dice_cover.F, dice_cover.precision, dice_cover.recall)
        output.write(l)

    output.close()


create_output("results/20/neg", "results/20/pos", "results/20_output.csv")
create_output("results/100/neg", "results/100/pos", "results/100_output.csv")
