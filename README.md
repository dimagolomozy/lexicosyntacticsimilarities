# **Extraction of Lexico-syntactic Similarities**

By: **Dima Golomozy & Eyal Ben-Simon**  
*Ben-Gurion University, Israel*

Implementation of MapReduce pattern for extraction of Lexico-syntactic similarities from the [Google Syntactic-Ngram resource](http://storage.googleapis.com/books/syntactic-ngrams/index.html)    
The algorithm is based on the paper of l. Dekang Lin and Patrick Pante: [DIRT – Discovery of Inference Rules from Text](http://www.egr.msu.edu/~jchai/QAPapers/InferenceRulesQA-Lin.pdf)  
More information can be found: [http://www.cs.bgu.ac.il/~dsp152/Assignments/Project](http://www.cs.bgu.ac.il/~dsp152/Assignments/Project)  

## Run
1. For all extracted features and path run the command in console:  

        $ ./client lexico <dp_min_count> <min_feature_num> <percent> <is_seq_file> <use_readable_output>
        
        args:
        
        <dp_min_count>          - The minimal count of the dependency paths
        <min_feature_num>       - The minimal number of (not empty) features per dependency path
        <percent>               - Number of percent to read from the input files
        <is_seq_file>           - Boolean, if the input are sequence files, or not
        <use_readable_output>   - Boolean, if the write to readable output 
        
2. For similarity between path run to command in console:

        $ ./client similarity <test_set_file_name> <>
                
        args:
        
        <test_set_file_name>    - The test set file name in the test set folder
        <input_path>            - The path to the input. should be the output of step3
   
## Steps  
*SORTER_STRING = (char)31*  

**Step 1:**  
        
        Mapper:
                Input:
                        Biarcs dataset of the syntactic NGRAM resource
                Output:
                        For each line create a path(s) with word in slot X and slot Y.
                        For each word in a slot, send it to the reducer with "path" as key and also "path/" as key
                Format:
                        path TAB word1/slotTag/PATH_SLOT_WORD/0/0
                        path/ TAB word1/slotTag/PATH_SLOT_WORD/0/0
                        
        Reducer:
                Input:
                        Output of Mapper (will be sorter) 
                Format:
                        path TAB word1/slotTag/PATH_SLOT_WORD/0/0
                        path/ TAB word1/slotTag/PATH_SLOT_WORD/0/0
                Output:
                        Calculates PATH_SLOT_STAR (= c(path, slot, *)) and the count of dependency path.
                        If the PATH_SLOT_STAR and the count are valid (smaller then DP_MIN_COUNT and MIN_FEATURE_NUM) continue.
                        For each valid path, write the path and the SyntacticSlotWritable with the word and add the PATH_SLOT_STAR to it.
                        The output is a path and a word SyntacticSlotWritable
                Format:
                        path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                     
**Step 2:**

       Mapper:
                Input:
                        The output of Step 1
                Format:
                        path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                Output:
                        For each path, creates 2 lines with:
                        key as WORD with SLOTTAG
                        and key as WORD with SLOTTAG and PATH
                        The value is a SyntacticSlotWritable untouched
                Format:
                        word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                        word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                        
       Combiner:
                Input:
                        The output of mapper
                Format:
                        word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                        word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                Output:
                        Combines same elements with the same key from the same myMapper.
                Format:
                        word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                        word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                        
       Reducer:
                Input:
                        Output of Mapper (will be sorter and after combiner) 
                Format:
                        word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                        word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
                Output:
                        Calculates the amount of the word in a given slot (= c(*, slot, word))
                        The output is a path and a word SyntacticSlotWritable
                Format:
                        path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
                      
                        
**Step 3:**

       Mapper:
                Input:
                        The output of Step 2
                Format:
                        path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
                Output:
                        Send to reducer the same line. but when writing to context, the mapper count
                        the STAR_SLOTX_STAR and STAR_SLOTY_STAR and send its in the end of the work to all the reducers
                        with (char)31 to calculate the c(*, slot, *)
                Format:
                        SORTER_STRING TAB Fake_SyntacticSlotWritable
                        path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
                        
       Partitioner:
                Splits the input with more reducers.
                Based on the hashCode of the path.
                And if the key is SORTER_STRING then returns all the reducers numbers one by one.
                        
       Reducer:
                Input:
                        Output of Mapper (will be sorter)) 
                Format:
                        SORTER_STRING TAB Fake_SyntacticSlotWritable
                        path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
                Output:
                        Calculates all the measures for all the slots in a given path.
                        Outputs a line of path with all the slots (slotX or slotY) with SPACE separated
                Format:
                        path TAB word1/slotTag/tfidf/dice/mi SPACE word2/slotTag/tfidf/dice/mi SPACE.....
                        
**Step 4:**

       Mapper:
                Input:
                     The output of Step 3
                     And the Test set from cache file and stores them to ArrayList<String[]> testSet
                Format of output Step 3:
                        path1 TAB word1/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word2/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
                        path2 TAB word3/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word4/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
                Format of TestSet:
                        path1 TAB path 2
                        path3 TAB path 1
                Output:
                        For each line from Step3, check is exists in the testSet array,
                        If exists, output the string from the testSet and add the path features with ID of the path 1 or 2
                Format:
                        path1/path2/"" TAB 1 word1/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word2/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
                        path1/path2/"" TAB 2 word3/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word4/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
                        
       Reducer:
                Input:
                        Output of Mapper (will be sorter)) 
                Format:
                        path1/path2/"" TAB 1 word1/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word2/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
                        path1/path2/"" TAB 2 word3/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word4/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
                Output:
                        Calculate all the measures between each pair: sim, cosine, cover
                Format:
                        path1 TAB path2 TAB sim/cosine/cover
                        
                        
                        
                        
