import com.amazonaws.regions.Regions;

public final class Common
{
    // Amazon data
    public static final String awsCredentials = "AwsCredentials.properties";
    public static final String SSH_KEY = "AWS_Eyal_Dima";

    // Instance properties
    public static final String HADOOP_VERSION = "2.4.0";
    public static final String INSTANCE_PLACEMENT = "us-east-1c";

    // S3 Directories
    public static final String S3_BUCKET_NAME = "dima-eyal-bucket-project";
    public static final String S3_JAR_DIR = "s3://" + S3_BUCKET_NAME + "/jars/";
    public static final String S3_INPUT_DIR = "s3://" + S3_BUCKET_NAME + "/input/";
    public static final String S3_TEST_SET_DIR = "s3://" + S3_BUCKET_NAME + "/testset/";
    public static final String S3_FLOWS_DIR = "s3://" + S3_BUCKET_NAME + "/flows/";

    // Elastic MapReduce
    public static final Regions EMR_REGION = Regions.US_EAST_1;

    // Local flow dir
    public static final String LOCAL_FLOWS_DIR = "flows/";
}
