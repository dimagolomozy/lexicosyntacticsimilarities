import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.*;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class lexico
{
    // Instance properties
    private static final int INSTANCE_COUNT = 5;
    private static final String INSTANCE_TYPE = InstanceType.M1Large.toString();

    // Flow properties
    private static final UUID RANDOM_UUID = UUID.randomUUID();
    private static final String FLOW_NAME =  lexico.class.getSimpleName() + "100%-flow-" + RANDOM_UUID.toString();
    private static final String SERVICE_ROLE = "EMR_DefaultRole";
    private static final String FLOW_ROLE = "EMR_EC2_DefaultRole";
    private static final String REDUCERS_AMOUNT = "10";

    // S3 Directories
    private static final String S3_FLOW_DIR = Common.S3_FLOWS_DIR + FLOW_NAME + "/";
    private static final String S3_FLOW_RESULT_DIR = S3_FLOW_DIR + "result" + "/";
    private static final String S3_FLOW_LOG_DIR = S3_FLOW_DIR + "log" + "/";
    private static final String S3_DATASET_TEMPLATE = "s3n://dsp152/syntactic-ngram/biarcs/biarcs.XX-of-99";

    // Amazon clients
    private static AmazonElasticMapReduceClient emr;
    private static AmazonS3Client s3;

    // printer
    private static String DETAILS_FILE = Common.LOCAL_FLOWS_DIR + FLOW_NAME;
    private static PrintWriter writer;

    /**
     * main function - starting point
     * @param args :
     * 	    args[0] = DPMinCount
     * 	    args[1] = MinFeatureNum
     * 	    args[2] = percent
     * 	    args[3] = Seq file or not... thanks Meni
     * 	    args[4] = use readable output
     */
    public static void main(String[] args)
    {
        try
        {
            // init the writer
            writer = new PrintWriter(DETAILS_FILE);

            myPrinter("# Start initialize flow " +
                    "\n>>> DPMinCount: " + args[0] +
                    "\n>>> MinFeatureNum: " + args[1] +
                    "\n>>> Percent: " + args[2] +
                    "\n>>> Sequence file: " + args[3] +
                    "\n>>> Use readable output: " + args[4]);

            // init the amazon EMR client
            initAmazonClients();

            String input = createInput(Integer.parseInt(args[2]));

            // initAmazon steps 1, 2, 3
            List<StepConfig> stepsConfig = initSteps(args[0], args[1], input, args[3], args[4]);

            // run the cluster with the steps
            runCluster(stepsConfig);

            // close the writer
            writer.close();

            // send details file to S3
            sendDetails();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void sendDetails()
    {
        System.out.println("# Sending details to S3 " +
                "\n>>> Details file: " + DETAILS_FILE);

        File detailsFile = new File(DETAILS_FILE);
        String key = detailsFile.getName();
        String folder = "flows/" + FLOW_NAME + "/";

        PutObjectRequest req = new PutObjectRequest(Common.S3_BUCKET_NAME, folder + key, detailsFile);
        s3.putObject(req);

        System.out.println(">>> Sent to " + Common.S3_FLOWS_DIR + FLOW_NAME);
    }

    private static String createInput(int percent)
    {
        final int dataSetSize = 98;
        final int sizeToRead = Math.round(((float)(dataSetSize * (percent / (double)100))));

        String output = "";
        HashSet<Integer> numbers = new HashSet<>();
        while (numbers.size() != sizeToRead)
        {
            int i = (int)(Math.random() * dataSetSize);
            if (numbers.contains(i))
                continue;

            output += S3_DATASET_TEMPLATE.replace("XX", String.format("%02d", i)) + ",";
            numbers.add(i);
        }

        return output.substring(0, output.length() - 1);
    }

    private static void initAmazonClients() throws Exception
    {
        InputStream awsCredentialInputStream = lexico.class.getResourceAsStream(Common.awsCredentials);
        AWSCredentials credentials = new PropertiesCredentials(awsCredentialInputStream);


        myPrinter("# Init EMR");
        emr = new AmazonElasticMapReduceClient(credentials);
        myPrinter(">>> Set region " + Common.EMR_REGION.toString());
        emr.setRegion(Region.getRegion(Common.EMR_REGION));

        myPrinter("# Init S3");
        s3 = new AmazonS3Client(credentials);
    }


    private static void runCluster(List<StepConfig> stepsConfig)
    {
        myPrinter("# Init instances");
        myPrinter(">>> Count: " + INSTANCE_COUNT +
                "\n>>> Type: " + INSTANCE_TYPE +
                "\n>>> Hadoop ver: " + Common.HADOOP_VERSION +
                "\n>>> SSH key: " + Common.SSH_KEY +
                "\n>>> Placement: " + Common.INSTANCE_PLACEMENT);

        JobFlowInstancesConfig instances = new JobFlowInstancesConfig()
                .withInstanceCount(INSTANCE_COUNT)
                .withMasterInstanceType(INSTANCE_TYPE)
                .withSlaveInstanceType(INSTANCE_TYPE)
                .withHadoopVersion(Common.HADOOP_VERSION)
                .withEc2KeyName(Common.SSH_KEY)
                .withKeepJobFlowAliveWhenNoSteps(false)
                .withPlacement(new PlacementType(Common.INSTANCE_PLACEMENT));


        myPrinter("# Init job flow request");
        myPrinter(">>> Name: " + FLOW_NAME +
                "\n>>> Steps count: " + stepsConfig.size() +
                "\n>>> Log uri: " + S3_FLOW_LOG_DIR +
                "\n>>> Service role: " + SERVICE_ROLE +
                "\n>>> Job flow role: " + FLOW_ROLE);

        RunJobFlowRequest runFlowRequest = new RunJobFlowRequest()
                .withName(FLOW_NAME)
                .withInstances(instances)
                .withSteps(stepsConfig)
                .withLogUri(S3_FLOW_LOG_DIR)
                .withServiceRole(SERVICE_ROLE)
                .withJobFlowRole(FLOW_ROLE);

        myPrinter("# Sending job flow: " + runFlowRequest.getName());
        RunJobFlowResult runJobFlowResult = emr.runJobFlow(runFlowRequest);

        String jobFlowId = runJobFlowResult.getJobFlowId();
        myPrinter(">>> Ran job flow with id: " + jobFlowId);
    }

    private static List<StepConfig> initSteps(String DPMinCount, String MinFeatureNum, String inputStep1,
                                              String isSeq, String useReadable)
    {
        System.out.println("# Init steps");

        final String outputStep1 = S3_FLOW_RESULT_DIR + "outputStep1";
        final String outputStep2 = S3_FLOW_RESULT_DIR + "outputStep2";
        final String outputStep3 = S3_FLOW_RESULT_DIR + "outputStep3";

        HadoopJarStepConfig[] hadoopJarsStep = new HadoopJarStepConfig[] {
                new HadoopJarStepConfig()   // step 1 in the map reduce application
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass(step1.class.getSimpleName())
                        .withArgs(inputStep1, outputStep1, DPMinCount, MinFeatureNum, REDUCERS_AMOUNT, isSeq, useReadable),

                new HadoopJarStepConfig()   // step 2 in the map reduce application
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass(step2.class.getSimpleName())
                        .withArgs(outputStep1, outputStep2, REDUCERS_AMOUNT, useReadable),

                new HadoopJarStepConfig()   // step 3 in the map reduce application
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass(step3.class.getSimpleName())
                        .withArgs(outputStep2, outputStep3, REDUCERS_AMOUNT, Boolean.toString(true)),
        };

        List<StepConfig> stepsConfig = new LinkedList<>();
        for (HadoopJarStepConfig hadoopJarStepConfig : hadoopJarsStep)
        {
            myPrinter(">>> Jar: " + hadoopJarStepConfig.getJar() +
                    "\n>>> Step class: " + hadoopJarStepConfig.getMainClass() +
                    "\n>>> Args: " + hadoopJarStepConfig.getArgs().toString());

            stepsConfig.add(new StepConfig()
                    .withName(hadoopJarStepConfig.getMainClass())
                    .withHadoopJarStep(hadoopJarStepConfig)
                    .withActionOnFailure("TERMINATE_JOB_FLOW"));
        }
        return stepsConfig;
    }

    private static void myPrinter(String line)
    {
        System.out.println(line);
        writer.println(line);
    }

}

