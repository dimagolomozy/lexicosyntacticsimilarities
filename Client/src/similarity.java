import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.*;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.UUID;

public class similarity
{
    // Instance properties
    private static final int INSTANCE_COUNT = 1;
    private static final String INSTANCE_TYPE = InstanceType.M1Large.toString();

    // Flow properties
    private static final UUID RANDOM_UUID = UUID.randomUUID();
    private static final String FLOW_NAME = similarity.class.getSimpleName() + "POS20%-flow-" + RANDOM_UUID.toString();
    private static final String SERVICE_ROLE = "EMR_DefaultRole";
    private static final String FLOW_ROLE = "EMR_EC2_DefaultRole";
    private static final String REDUCERS_AMOUNT = "1";

    // S3 Directories
    private static final String S3_FLOW_DIR = Common.S3_FLOWS_DIR + FLOW_NAME + "/";
    private static final String S3_FLOW_RESULT_DIR = S3_FLOW_DIR + "result" + "/";
    private static final String S3_FLOW_LOG_DIR = S3_FLOW_DIR + "log" + "/";

    // Amazon clients
    private static AmazonElasticMapReduceClient emr;
    private static AmazonS3Client s3;

    // printer
    private static String DETAILS_FILE = Common.LOCAL_FLOWS_DIR + FLOW_NAME;
    private static PrintWriter writer;

    /**
     * main function - starting point
     * @param args :
     *      args[0] = test set file name in the Test Set dir
     * 	    args[1] = input path in "Flows" dir
     */
    public static void main(String[] args)
    {
        try
        {
            // init the writer
            writer = new PrintWriter(DETAILS_FILE);

            myPrinter("# Start initialize flow " +
                    "\n>>> Test set file: " + args[0] +
                    "\n>>> Input path: " + args[1]);

            // init the amazon EMR client
            initAmazonClients();

            // initAmazon steps 4
            StepConfig stepsConfig = initSteps(args[0], args[1]);

            // run the cluster with the step
            runCluster(stepsConfig);

            // close the writer
            writer.close();

            // send details file t5o S3
            sendDetails();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void sendDetails()
    {
        System.out.println("# Sending details to S3 " +
                "\n>>> Details file: " + DETAILS_FILE);

        File detailsFile = new File(DETAILS_FILE);
        String key = detailsFile.getName();
        String folder = "flows/" + FLOW_NAME + "/";

        PutObjectRequest req = new PutObjectRequest(Common.S3_BUCKET_NAME, folder + key, detailsFile);
        s3.putObject(req);

        System.out.println(">>> Sent to " + Common.S3_FLOWS_DIR + FLOW_NAME);
    }

    private static void initAmazonClients() throws Exception
    {
        InputStream awsCredentialInputStream = lexico.class.getResourceAsStream(Common.awsCredentials);
        AWSCredentials credentials = new PropertiesCredentials(awsCredentialInputStream);


        myPrinter("# Init EMR");
        emr = new AmazonElasticMapReduceClient(credentials);
        myPrinter(">>> Set region " + Common.EMR_REGION.toString());
        emr.setRegion(Region.getRegion(Common.EMR_REGION));

        myPrinter("# Init S3");
        s3 = new AmazonS3Client(credentials);
    }


    private static void runCluster(StepConfig stepsConfig)
    {
        myPrinter("# Init instances");
        myPrinter(">>> Count: " + INSTANCE_COUNT +
                "\n>>> Type: " + INSTANCE_TYPE +
                "\n>>> Hadoop ver: " + Common.HADOOP_VERSION +
                "\n>>> SSH key: " + Common.SSH_KEY +
                "\n>>> Placement: " + Common.INSTANCE_PLACEMENT);

        JobFlowInstancesConfig instances = new JobFlowInstancesConfig()
                .withInstanceCount(INSTANCE_COUNT)
                .withMasterInstanceType(INSTANCE_TYPE)
                .withSlaveInstanceType(INSTANCE_TYPE)
                .withHadoopVersion(Common.HADOOP_VERSION)
                .withEc2KeyName(Common.SSH_KEY)
                .withKeepJobFlowAliveWhenNoSteps(false)
                .withPlacement(new PlacementType(Common.INSTANCE_PLACEMENT));


        myPrinter("# Init job flow request");
        myPrinter(">>> Name: " + FLOW_NAME +
                "\n>>> Steps count: 1" +
                "\n>>> Log uri: " + S3_FLOW_LOG_DIR +
                "\n>>> Service role: " + SERVICE_ROLE +
                "\n>>> Job flow role: " + FLOW_ROLE);

        RunJobFlowRequest runFlowRequest = new RunJobFlowRequest()
                .withName(FLOW_NAME)
                .withInstances(instances)
                .withSteps(stepsConfig)
                .withLogUri(S3_FLOW_LOG_DIR)
                .withServiceRole(SERVICE_ROLE)
                .withJobFlowRole(FLOW_ROLE);

        myPrinter("# Sending job flow: " + runFlowRequest.getName());
        RunJobFlowResult runJobFlowResult = emr.runJobFlow(runFlowRequest);

        String jobFlowId = runJobFlowResult.getJobFlowId();
        myPrinter(">>> Ran job flow with id: " + jobFlowId);
    }

    private static StepConfig initSteps(String testSetFile, String inputPath)
    {
        System.out.println("# Init steps");

        final String testSetFilePath = Common.S3_TEST_SET_DIR + testSetFile;
        final String inputStep4 = Common.S3_FLOWS_DIR + inputPath;
        final String outputStep4 = S3_FLOW_RESULT_DIR;

        HadoopJarStepConfig hadoopJarStep = new HadoopJarStepConfig()   // step 4 in the map reduce application
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass(step4.class.getSimpleName())
                        .withArgs(inputStep4, outputStep4, testSetFilePath, REDUCERS_AMOUNT);

        myPrinter(">>> Jar: " + hadoopJarStep.getJar() +
                "\n>>> Step class: " + hadoopJarStep.getMainClass() +
                "\n>>> Args: " + hadoopJarStep.getArgs().toString());

        return new StepConfig()
                .withName(hadoopJarStep.getMainClass())
                .withHadoopJarStep(hadoopJarStep)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
    }

    private static void myPrinter(String line)
    {
        System.out.println(line);
        writer.println(line);
    }
}
