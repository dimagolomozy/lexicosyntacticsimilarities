import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import slots.Slot;
import syntactic.PathsGenerator;
import syntactic.SyntacticPath;
import syntactic.SyntacticToken;
import writables.SyntacticSlotWritable;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class step1
{
    private final static String DP_MIN_COUNT_KEY = "dp.min.PATH_SLOT_WORD";
    private final static String MIN_FEATURE_NUM_KEY = "min.feature.num";
    private final static String READABLE_OUTPUT = "readable";
    private final static String USE_READABLE_OUTPUT = "use.readable.output";
    private final static String SORTER_STRING = "" + (char)31;

    /**
     * Input:
     *      Biarcs dataset of the syntactic NGRAM resource
     *  Format:
     *      head_word TAB syntactic-ngram TAB total_count TAB counts_by_year
     *
     *      The counts_by_year format is a tab-separated list of year<comma>PATH_SLOT_WORD items.
     *      Years are sorted in ascending order, and only years with non-zero counts are included.
     *
     *      The syntactic-ngram format is a space-separated list of tokens,
     *      each token format is “word/pos-tag/dep-label/head-index”.
     *
     *      The word field can contain any non-whitespace character.
     *      The other fields can contain any non-whitespace character except for ‘/’.
     *
     *      pos-tag is a Penn-Treebank part-of-speech tag.
     *
     *      dep-label is a stanford-basic-dependencies label.
     *
     *      head-index is an integer, pointing to the head of the current token.
     *
     * Output:
     *      For each line create a path(s) with word in slot X and slot Y.
     *      For each word in a slot, send it to the reducer with "path" as key and also "path/" as key
     *
     *  Format:
     *      path TAB word1/slotTag/PATH_SLOT_WORD/0/0
     *      path/ TAB word1/slotTag/PATH_SLOT_WORD/0/0
     */
    private static class myMapper extends Mapper<LongWritable, Text, Text, SyntacticSlotWritable>
    {
        private static final PathsGenerator pathsGenerator = new PathsGenerator();

        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException,  InterruptedException
        {
            // split the line by \t (TAB)
            StringTokenizer line = new StringTokenizer(value.toString(), "\t");
            // get the head WORD (unused)
            String headWord = line.nextToken();
            // get the syntactic ngram
            String syntacticNgram = line.nextToken();
            // get the PATH_SLOT_WORD of the syntactic ngram
            String count = line.nextToken();
            // clear pathsGenerator
            pathsGenerator.clear();

            // split the syntactic-ngram by " " (SPACE)
            StringTokenizer token = new StringTokenizer(syntacticNgram, " ");
            while (token.hasMoreTokens())
            {
                    String[] str = token.nextToken().split("/");
                /*
                    str[0] = word
                    str[1] = pos-tag
                    str[2] = dep-Lable
                    str[3] = head-index
                 */

                // create and insert a syntactic token to the PathsGenerator
                try
                {
                    SyntacticToken syntacticToken = new SyntacticToken(str[0], str[1], str[3]);
                    pathsGenerator.insert(syntacticToken);
                }
                catch (Exception e) { return; }
            }

            // get list of all the syntactic paths
            LinkedList<SyntacticPath> pathsList = pathsGenerator.generatePaths();

            // all good. lets write them to the output!
            for (SyntacticPath synPath : pathsList)
            {
                String path = synPath.getPath();
                Slot slotX = synPath.getSlotX();
                Slot slotY = synPath.getSlotY();
                double PATH_SLOT_WORD = Integer.parseInt(count);

                writeToContext(context, path, slotX, PATH_SLOT_WORD);
                writeToContext(context, path, slotY, PATH_SLOT_WORD);
            }
        }

        private void writeToContext(Context context, String path, Slot slot, double PATH_SLOT_WORD) throws IOException, InterruptedException
        {
            // create value
            SyntacticSlotWritable syntacticSlotWritable =
                    new SyntacticSlotWritable(slot.getWord(), slot.getSlotTag(), PATH_SLOT_WORD, 0, 0);

            // create key: path
            Text key = new Text(path);
            context.write(key, syntacticSlotWritable);

            // create key: path/
            key.append(SORTER_STRING.getBytes(), 0, 1);
            context.write(key, syntacticSlotWritable);
        }
    }

    /**
     * Splits the input with more reducers.
     * Based on the hashCode of the key (the path), so the same path wil lgo to the same reducer
     */
    private static class myPartitioner extends Partitioner<Text, SyntacticSlotWritable>
    {
        @Override
        public int getPartition(Text key, SyntacticSlotWritable value, int numPartitions)
        {
            String strKey = key.toString();

            // get the only the path
            String path = strKey.contains(SORTER_STRING) ? strKey.substring(0, strKey.length() - 1) : strKey;

            return Math.abs(path.hashCode()) % numPartitions;
        }
    }

    /**
     * Input:
     *      The output of myMapper
     *      Will be sorted.
     *  Format:
     *      path TAB word1/slotTag/PATH_SLOT_WORD/0/0
     *      path/ TAB word1/slotTag/PATH_SLOT_WORD/0/0
     *
     * Output:
     *      Calculates PATH_SLOT_STAR (= c(path, slot, *)) and the count of dependency path.
     *      If the PATH_SLOT_STAR and the count are valid (smaller then DP_MIN_COUNT and MIN_FEATURE_NUM) continue.
     *      For each valid path, write the path and the SyntacticSlotWritable with the word and add the PATH_SLOT_STAR to it.
     *      The output is a path and a word SyntacticSlotWritable
     *
     *  Format:
     *      path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *
     */
    private static class myReducer extends Reducer<Text, SyntacticSlotWritable, Text, SyntacticSlotWritable>
    {
        private static int MIN_FEATURE_NUM;
        private static int DP_MIN_COUNT;
        private static int FEATURE_COUNT = 0;
        private static double PATH_SLOT_STAR = 0;

        private static MultipleOutputs MOS;
        private static boolean USE_MOS;

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException
        {
            MOS.close();
            super.cleanup(context);
        }

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            super.setup(context);
            MOS = new MultipleOutputs<>(context);

            USE_MOS = context.getConfiguration().getBoolean(USE_READABLE_OUTPUT, false);
            DP_MIN_COUNT = context.getConfiguration().getInt(DP_MIN_COUNT_KEY, 0);
            MIN_FEATURE_NUM = context.getConfiguration().getInt(MIN_FEATURE_NUM_KEY, 0);
        }

        @Override
        public void reduce(Text key, Iterable<SyntacticSlotWritable> values, Context context)
                throws IOException, InterruptedException
        {
            String strKey = key.toString();

            if (strKey.contains(SORTER_STRING))
            {   // means: path/ TAB word/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD

                if ((FEATURE_COUNT / 2) < MIN_FEATURE_NUM || (PATH_SLOT_STAR / 2) < DP_MIN_COUNT)
                    return;

                // create new key with only the path
                Text path = new Text(strKey.substring(0, strKey.length() - 1));

                // foreach syntacticSlot add the PATH_SLOT_STAR and write it out
                for (SyntacticSlotWritable syntacticSlotWritable : values)
                {
                    // add PATH_SLOT_STAR
                    if (!syntacticSlotWritable.isSlotUntagged())
                        syntacticSlotWritable.setPATH_SLOT_STAR(PATH_SLOT_STAR);

                    // write it to seq file
                    context.write(path, syntacticSlotWritable);

                    // write it to readable file
                    if (USE_MOS)
                        MOS.write(READABLE_OUTPUT, path, syntacticSlotWritable, "_"  + READABLE_OUTPUT);
                }
            }
            else
            {   // means : path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOW_STAR/STAR_SLOT_WORD
                // so count total PATH_SLOT_STAR and totalCount

                HashSet<SyntacticSlotWritable> words = new HashSet<>();

                FEATURE_COUNT = 0;
                PATH_SLOT_STAR = 0;
                // calculate c(path, slot, *)
                for (SyntacticSlotWritable syntacticSlotWritable : values)
                {
                    if (!syntacticSlotWritable.isSlotUntagged())
                    {
                        PATH_SLOT_STAR += syntacticSlotWritable.getPATH_SLOT_WORD();

                        if (words.contains(syntacticSlotWritable))
                            continue;

                        words.add(syntacticSlotWritable);
                        FEATURE_COUNT++;
                    }
                }
            }
        }
    }


    /**
     * Main of Step 1 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - DPMinCount
     *            args[3] - MinFeatureNum
     *            args[4] - Reducers amount
     *            args[5] - is Seq file
     *            args[6] - use Readable
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        System.out.println("# Init Step 1");

        Configuration conf = new Configuration();
        conf.setInt(DP_MIN_COUNT_KEY, Integer.parseInt(args[2]));
        conf.setInt(MIN_FEATURE_NUM_KEY, Integer.parseInt(args[3]));
        conf.setBoolean(USE_READABLE_OUTPUT, Boolean.parseBoolean(args[6]));

        Job job = new Job(conf, "Step 1");
        job.setNumReduceTasks(Integer.parseInt(args[4]));
        job.setJarByClass(step1.class);
        job.setMapperClass(step1.myMapper.class);
        job.setPartitionerClass(step1.myPartitioner.class);
        job.setReducerClass(step1.myReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(SyntacticSlotWritable.class);

        if (Boolean.parseBoolean(args[5]))
            job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        MultipleOutputs.addNamedOutput(job, READABLE_OUTPUT, TextOutputFormat.class, Text.class, Text.class);
        FileInputFormat.addInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.out.println("# Starting Step 1");
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

