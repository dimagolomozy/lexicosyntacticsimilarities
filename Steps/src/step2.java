import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import writables.SyntacticSlotWritable;
import writables.TripleKey;

import java.io.IOException;

public class step2
{
    private final static String READABLE_OUTPUT = "readable";
    private final static String USE_READABLE_OUTPUT = "use.readable.output";


    /**
     * Input:
     *      Output of Step 1
     *  Format:
     *      path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *
     * Output:
     *      For each path, creates 2 lines with:
     *      key as WORD with SLOTTAG
     *      and key as WORD with SLOTTAG and PATH
     *      The value is a SyntacticSlotWritable untouched
     *  Format:
     *      word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *      word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     */
    private static class myMapper extends Mapper<Text, SyntacticSlotWritable, TripleKey, SyntacticSlotWritable>
    {
        @Override
        public void map(Text path, SyntacticSlotWritable syntacticSlotWritable, Context context)
                throws IOException,  InterruptedException
        {
            // create key: word/slotTag/path
            TripleKey tripleKey = new TripleKey(syntacticSlotWritable.getWord(), syntacticSlotWritable.getSlotTag(), path.toString());
            context.write(tripleKey, syntacticSlotWritable);

            // create key: word/slotTag/""
            tripleKey.setKey3("");
            context.write(tripleKey, syntacticSlotWritable);
        }
    }

    /**
     * Splits the input with more reducers.
     * Based on the hashCode of the key (the word), so the same word goes to the same reducer
     */
    private static class myPartitioner extends Partitioner<TripleKey, SyntacticSlotWritable>
    {
        @Override
        public int getPartition(TripleKey key, SyntacticSlotWritable value, int numPartitions)
        {
            return key.getKey1HashCode() % numPartitions;
        }
    }

    /**
     * Input:
     *      The output of myMapper
     *  Format:
     *      word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *      word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *
     * Output:
     *      Combines same elements with the same key from the same myMapper.
     *
     *  Format:
     *      word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *      word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     */
    private static class myCombiner extends Reducer<TripleKey, SyntacticSlotWritable, TripleKey, SyntacticSlotWritable>
    {
        @Override
        public void reduce(TripleKey key, Iterable<SyntacticSlotWritable> values, Context context)
                throws IOException, InterruptedException
        {
            double PATH_SLOT_WORD = 0;
            double PATH_SLOT_STAR = 0;
            String word = "";
            String slotTag = "";

            // foreach same syntacticSlot calculate the total of PATH_SLOT_WORD and write it back.
            for (SyntacticSlotWritable syntacticSlotWritable : values)
            {
                PATH_SLOT_WORD += syntacticSlotWritable.getPATH_SLOT_WORD();
                if (word.isEmpty())
                    word = syntacticSlotWritable.getWord();
                if (slotTag.isEmpty())
                    slotTag = syntacticSlotWritable.getSlotTag();
                if (PATH_SLOT_STAR == 0)
                    PATH_SLOT_STAR = syntacticSlotWritable.getPATH_SLOT_STAR();
            }

            // write it back to context
            context.write(key, new SyntacticSlotWritable(word, slotTag, PATH_SLOT_WORD, PATH_SLOT_STAR, 0));
        }
    }


    /**
     * Input:
     *      The output of myMapper after the myCombiner.
     *      Will be sorted.
     *  Format:
     *      word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *      word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0
     *
     * Output:
     *      Calculates the amount of the word in a given slot (= c(*, slot, word))
     *      The output is a path and a word SyntacticSlotWritable
     *  Format:
     *      path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
     *
     */
    private static class myReducer extends Reducer<TripleKey, SyntacticSlotWritable, Text, SyntacticSlotWritable>
    {
        private static double STAR_SLOT_WORD = 0;

        private static MultipleOutputs MOS;
        private static boolean USE_MOS;

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException
        {
            MOS.close();
            super.cleanup(context);
        }

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            super.setup(context);
            MOS = new MultipleOutputs<>(context);

            USE_MOS = context.getConfiguration().getBoolean(USE_READABLE_OUTPUT, false);
        }

        @Override
        public void reduce(TripleKey key, Iterable<SyntacticSlotWritable> values, Context context)
                throws IOException, InterruptedException
        {
            if (key.getKey3().isEmpty())
            {   //means:  word1/slotTag/"" TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0

                // calculate c(*, slot, word)
                STAR_SLOT_WORD = 0;
                for (SyntacticSlotWritable syntacticSlotWritable : values)
                {
                    STAR_SLOT_WORD += syntacticSlotWritable.getPATH_SLOT_WORD();
                }
            }
            else
            {   // means: word1/slotTag/path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/0

                double PATH_SLOT_STAR = 0;
                double PATH_SLOT_WORD = 0;
                // calculate the total of PATH_SLOT_WORD
                for (SyntacticSlotWritable syntacticSlotWritable : values)
                {
                    PATH_SLOT_WORD += syntacticSlotWritable.getPATH_SLOT_WORD();
                    if (PATH_SLOT_STAR == 0)
                        PATH_SLOT_STAR = syntacticSlotWritable.getPATH_SLOT_STAR();
                }

                // create new key
                Text path = new Text(key.getKey3());
                // create new value
                SyntacticSlotWritable syntacticSlotWritable =
                        new SyntacticSlotWritable(key.getKey1(), key.getKey2(), PATH_SLOT_WORD, PATH_SLOT_STAR, STAR_SLOT_WORD);

                // write it to seq file
                context.write(path, syntacticSlotWritable);

                // write it to readable file
                if (USE_MOS)
                    MOS.write(READABLE_OUTPUT, path, syntacticSlotWritable, "_"  + READABLE_OUTPUT);
            }
        }
    }


    /**
     * Main of Step 2 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - Reducers amount
     *            args[3] - use Readable
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        System.out.println("# Init Step 2");
        Configuration conf = new Configuration();
        conf.setBoolean(USE_READABLE_OUTPUT, Boolean.parseBoolean(args[3]));

        Job job = new Job(conf, "Step 2");
        job.setNumReduceTasks(Integer.parseInt(args[2]));
        job.setJarByClass(step2.class);
        job.setMapOutputKeyClass(TripleKey.class);
        job.setMapOutputValueClass(SyntacticSlotWritable.class);
        job.setMapperClass(step2.myMapper.class);
        job.setCombinerClass(step2.myCombiner.class);
        job.setPartitionerClass(step2.myPartitioner.class);
        job.setReducerClass(step2.myReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(SyntacticSlotWritable.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        MultipleOutputs.addNamedOutput(job, READABLE_OUTPUT, TextOutputFormat.class, Text.class, Text.class);
        FileInputFormat.addInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.out.println("# Starting Step 2");
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

