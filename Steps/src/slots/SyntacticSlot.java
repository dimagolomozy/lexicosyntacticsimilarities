package slots;

public class SyntacticSlot extends Slot
{
    protected double PATH_SLOT_WORD;
    protected double PATH_SLOT_STAR;
    protected double STAR_SLOT_WORD;

    protected SyntacticSlot()
    {
        // nothing
    }

    protected SyntacticSlot(String word, String slotTag, double PATH_SLOT_WORD, double PATH_SLOT_STAR, double STAR_SLOT_WORD)
    {
        super(word, slotTag);
        this.PATH_SLOT_WORD = PATH_SLOT_WORD;
        this.PATH_SLOT_STAR = PATH_SLOT_STAR;
        this.STAR_SLOT_WORD = STAR_SLOT_WORD;
    }

    @Override
    public String toString()
    {
        return super.toString() + DELIMITER +
                PATH_SLOT_WORD + DELIMITER +
                PATH_SLOT_STAR + DELIMITER +
                STAR_SLOT_WORD;
    }

    public double getPATH_SLOT_WORD()
    {
        return PATH_SLOT_WORD;
    }

    public double getPATH_SLOT_STAR()
    {
        return PATH_SLOT_STAR;
    }

    public double getSTAR_SLOT_WORD()
    {
        return STAR_SLOT_WORD;
    }

    public void setPATH_SLOT_STAR(double PATH_SLOT_STAR)
    {
        this.PATH_SLOT_STAR = PATH_SLOT_STAR;
    }

    public void setPATH_SLOT_WORD(double PATH_SLOT_WORD)
    {
        this.PATH_SLOT_WORD = PATH_SLOT_WORD;
    }

    public void setSTAR_SLOT_WORD(double STAR_SLOT_WORD)
    {
        this.STAR_SLOT_WORD = STAR_SLOT_WORD;
    }

}
