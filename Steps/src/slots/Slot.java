package slots;

public class Slot
{
    public static String DELIMITER = "/";
    protected enum Tag
    {
        SLOT_X,
        SLOT_Y,
        UNTAGGED,
    }

    protected String word;
    protected Tag tag;

    protected Slot()
    {
        // nothing
    }

    protected Slot(String word, String tag)
    {
        this.word = word;
        try
        {
            this.tag = Tag.valueOf(tag);
        }
        catch (IllegalArgumentException ex)
        {
            this.tag = Tag.UNTAGGED;
        }
    }

    public String getWord()
    {
        return word;
    }

    public String getSlotTag() { return this.tag.toString(); }

    public boolean isSlotX()
    {
        return (tag == Tag.SLOT_X);
    }

    public boolean isSlotY()
    {
        return (tag == Tag.SLOT_Y);
    }

    public boolean isSlotUntagged()
    {
        return (tag == Tag.UNTAGGED);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (!(obj instanceof Slot))
            return false;

        Slot other = (Slot)obj;

        return (this.tag == other.tag) && (this.word.equals(other.word));
    }

    public int compareTo(Slot other)
    {
        if (other == null)
            return 1;

        int comp = tag.compareTo(other.tag);
        if (comp == 0)
            return word.compareTo(other.word);
        else
            return comp;
    }

    @Override
    public String toString()
    {
        return word + DELIMITER + tag.toString();
    }

    @Override
    public int hashCode()
    {
        return Math.abs(word.hashCode());
    }

    public static class SlotX extends Slot
    {
        public SlotX(String word)
        {
            super(word, Tag.SLOT_X.name());
        }
    }

    public static class SlotY extends Slot
    {
        public SlotY(String word)
        {
            super(word, Tag.SLOT_Y.name());
        }
    }

}
