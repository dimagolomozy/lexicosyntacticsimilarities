package slots;

public class FeatureSlot extends Slot
{
    protected double tfidf;
    protected double dice;
    protected double mi;

    protected FeatureSlot()
    {
        // nothing
    }

    public FeatureSlot(String word, String slotTag, double tfidf, double dice, double mi)
    {
        super(word, slotTag);
        this.tfidf = tfidf;
        this.dice = dice;
        this.mi = mi;
    }

    public double getTfidf()
    {
        return tfidf;
    }

    public double getDice()
    {
        return dice;
    }

    public double getMi()
    {
        return mi;
    }

    @Override
    public String toString()
    {
        return super.toString() + DELIMITER + tfidf + DELIMITER + dice + DELIMITER + mi;
    }


}
