import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import syntactic.Measures;
import syntactic.Triples;
import writables.FeatureSlotArrayWritable;
import writables.FeatureSlotWritable;
import writables.TripleKey;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class step4
{
    private static String TEST_SET_CACHE_FILE_NAME = "test.set";

    /**
     * Input:
     *      The output of Step 3
     *      Reads the Test set from cache file and stores them to ArrayList<String[]> testSet
     *  Format of output Step 3:
     *      path1 TAB word1/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word2/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
     *      path2 TAB word3/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word4/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
     *  Format of TestSet:
     *      path1 TAB path 2
     *      path3 TAB path 1
     *
     * Output:
     *      For each line from step3, check is exists in the testSet array,
     *      if exists, output the string from the testSet and add the path features with ID of the path 1 or 2
     *  Format:
     *      path1/path2/"" TAB 1 word1/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word2/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
     *      path1/path2/"" TAB 2 word3/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word4/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
     */
    private static class myMapper extends Mapper<Text, FeatureSlotArrayWritable, TripleKey, FeatureSlotArrayWritable>
    {
        private ArrayList<String[]> testSet = new ArrayList<>();

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            super.setup(context);

            // get the files
            URI[] cacheFiles = context.getCacheFiles();

            if (cacheFiles != null && cacheFiles.length > 0)
            {
                for (URI cachePathUri : cacheFiles)
                {
                    Path cachePath = new Path(cachePathUri.getFragment());
                    if (cachePath.getName().equals(TEST_SET_CACHE_FILE_NAME))
                    {
                        // read test set
                        readTestSetFile(cachePath.toString());
                        break;
                    }
                }
            }
        }

        private void readTestSetFile(String filePath) throws IOException
        {
            List<String> lines = FileUtils.readLines(new File(filePath));
            for (String line : lines)
                testSet.add(line.split("\t"));
        }

        @Override
        public void map(Text key, FeatureSlotArrayWritable featureSlotArrayWritable, Context context)
                throws IOException, InterruptedException
        {
            String path = key.toString();

            for (String[] testPaths : testSet)
            {
                TripleKey tripleKey = new TripleKey(testPaths[0], testPaths[1], "");
                if (path.equals(testPaths[0]))
                {
                    featureSlotArrayWritable.setPathID(1);
                    context.write(tripleKey, featureSlotArrayWritable);
                }
                if (path.equals(testPaths[1]))
                {
                    featureSlotArrayWritable.setPathID(2);
                    context.write(tripleKey, featureSlotArrayWritable);
                }
            }
        }
    }

    /**
     * Splits the input with more reducers.
     * Based on the hashCode of the path1.
     */
    private static class myPartitioner extends Partitioner<TripleKey, FeatureSlotArrayWritable>
    {
        @Override
        public int getPartition(TripleKey key, FeatureSlotArrayWritable value, int numPartitions)
        {
            return key.getKey1HashCode() % numPartitions;
        }
    }

    /**
     * Input:
     *      Output of the mapper
     *  Format:
     *      path1/path2/"" TAB 1 word1/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word2/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
     *      path1/path2/"" TAB 2 word3/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE word4/slotTag/PATH_SLOT_WORD/STAR_SLOT_WORD SPACE.....
     *
     * Output:
     *      Calculate all the measures between each pair: sim, cosine, cover
     *
     * Format:
     *      path1 TAB path2 TAB sim/cosine/cover
     */
    private static class myReducer extends Reducer<TripleKey, FeatureSlotArrayWritable, Text, Text>
    {
        @Override
        public void reduce(TripleKey key, Iterable<FeatureSlotArrayWritable> values, Context context)
                throws IOException,  InterruptedException
        {
            // create Triples 1 and 2
            Triples triples1 = new Triples(key.getKey1());
            Triples triples2 = new Triples(key.getKey2());

            // get all the features
            for (FeatureSlotArrayWritable featureSlotArrayWritable : values)
            {
                FeatureSlotWritable[] featureSlotWritables = featureSlotArrayWritable.get();
                if (featureSlotArrayWritable.getPathID() == 1)
                {
                    for (FeatureSlotWritable featureSlotWritable : featureSlotWritables)
                        triples1.insert(featureSlotWritable);
                }
                else
                {
                    for (FeatureSlotWritable featureSlotWritable : featureSlotWritables)
                        triples2.insert(featureSlotWritable);
                }
            }

            // if one of them is empty means that the empty triple was not found
            if (triples1.isEmpty() || triples2.isEmpty())
                return;

            // calculate measures
            double sim = Measures.sim(triples1, triples2);
            double cosine = Measures.cosine(triples1, triples2);
            double cover = Measures.cover(triples1, triples2);

            if (sim == Double.NaN || cosine == Double.NaN || cover == Double.NaN)
            {
                System.err.println("sim=" + sim + " cosine=" + cosine + " cover=" + cover);
                System.err.println(triples1);
                System.err.println(triples2);
            }

            // create new key and new value as Text
            Text newKey = new Text(key.getKey1() + "\t" + key.getKey2());
            Text newValue = new Text("" + sim + "/" + cosine + "/" + cover);

            // write it out
            context.write(newKey, newValue);
        }
    }

    /**
     * Main of Step 4 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - input Test Set
     *            args[3] - Reducers amount
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        System.out.println("# Init Step 4");
        Configuration conf = new Configuration();
        Job job = new Job(conf, "Step 4");
        job.setNumReduceTasks(Integer.parseInt(args[3]));
        job.addCacheFile(new URI(args[2] + "#" + TEST_SET_CACHE_FILE_NAME));
        job.setMapOutputKeyClass(TripleKey.class);
        job.setMapOutputValueClass(FeatureSlotArrayWritable.class);
        job.setJarByClass(step4.class);
        job.setMapperClass(myMapper.class);
        job.setPartitionerClass(step4.myPartitioner.class);
        job.setReducerClass(step4.myReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.out.println("# Starting Step 4");
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }


}
