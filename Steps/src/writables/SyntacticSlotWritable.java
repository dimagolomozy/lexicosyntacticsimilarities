package writables;

import org.apache.hadoop.io.WritableComparable;
import slots.SyntacticSlot;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class SyntacticSlotWritable extends SyntacticSlot implements WritableComparable<SyntacticSlotWritable>
{
    private SyntacticSlotWritable()
    {
        // nothing
    }

    public SyntacticSlotWritable(String word, String slotTag, String PATH_SLOT_WORD, String PATH_SLOT_STAR, String STAR_SLOT_WORD)
    {
        this(word, slotTag, Double.parseDouble(PATH_SLOT_WORD), Double.parseDouble(PATH_SLOT_STAR), Double.parseDouble(STAR_SLOT_WORD));
    }

    public SyntacticSlotWritable(String word, String slotTag, double PATH_SLOT_WORD, double PATH_SLOT_STAR, double STAR_SLOT_WORD)
    {
        super(word, slotTag, PATH_SLOT_WORD, PATH_SLOT_STAR, STAR_SLOT_WORD);
    }

    @Override
    public int compareTo(SyntacticSlotWritable other)
    {
        return super.compareTo(other);
    }

    @Override
    public void write(DataOutput out) throws IOException
    {
        out.writeUTF(word);
        out.writeUTF(tag.name());
        out.writeDouble(PATH_SLOT_WORD);
        out.writeDouble(PATH_SLOT_STAR);
        out.writeDouble(STAR_SLOT_WORD);
    }

    @Override
    public void readFields(DataInput in) throws IOException
    {
        word = in.readUTF();
        tag = Tag.valueOf(in.readUTF());
        PATH_SLOT_WORD = in.readDouble();
        PATH_SLOT_STAR = in.readDouble();
        STAR_SLOT_WORD = in.readDouble();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new SyntacticSlotWritable(word, tag.toString(), PATH_SLOT_WORD, PATH_SLOT_STAR, STAR_SLOT_WORD);
    }
}