package writables;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TripleKey implements WritableComparable<TripleKey>
{
    public static String DELIMITER = "/";

    private String key1;
    private String key2;
    private String key3;

    public TripleKey()
    {
        this("", "" ,"");
    }

    public TripleKey(String key1, String key2, String key3)
    {
        this.key1 = key1;
        this.key2 = key2;
        this.key3 = key3;
    }

    public String getKey1()
    {
        return key1;
    }

    public void setKey1(String key1)
    {
        this.key1 = key1;
    }

    public String getKey2()
    {
        return key2;
    }

    public void setKey2(String key2)
    {
        this.key2 = key2;
    }

    public String getKey3()
    {
        return key3;
    }

    public void setKey3(String key3)
    {
        this.key3 = key3;
    }

    public int getKey1HashCode()
    {
        return Math.abs(key1.hashCode());
    }

    public int getKey2HashCode()
    {
        return Math.abs(key2.hashCode());
    }

    public int getKey3HashCode()
    {
        return Math.abs(key3.hashCode());
    }

    @Override
    public void write(DataOutput out) throws IOException
    {
        out.writeUTF(key1);
        out.writeUTF(key2);
        out.writeUTF(key3);
    }

    @Override
    public void readFields(DataInput in) throws IOException
    {
        key1 = in.readUTF();
        key2 = in.readUTF();
        key3 = in.readUTF();
    }

    @Override
    public int compareTo(TripleKey other)
    {
        int comp;
        if ((comp = key1.compareTo(other.key1)) != 0)
            return comp;
        else if ((comp = key2.compareTo(other.key2)) != 0)
            return comp;
        else return key3.compareTo(other.key3);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (!(obj instanceof TripleKey))
            return false;

        TripleKey other = (TripleKey) obj;

        return (this.key1.equals(other.key1)) && (this.key2.equals(other.key2)) && (this.key3.equals(other.key3));

    }

    @Override
    public String toString()
    {
        return key1 + "/" + key2 + "/" + key3;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new TripleKey(key1, key2, key3);
    }
}
