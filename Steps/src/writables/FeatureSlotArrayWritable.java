package writables;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

public class FeatureSlotArrayWritable extends ArrayWritable
{
    private int pathID = 0;
    public FeatureSlotArrayWritable() { super(FeatureSlotWritable.class); }

    public FeatureSlotArrayWritable(Writable[] values)
    {
        super(FeatureSlotWritable.class, values);
    }

    public int getPathID()
    {
        return pathID;
    }

    public void setPathID(int pathID)
    {
        this.pathID = pathID;
    }

    @Override
    public void readFields(DataInput in) throws IOException
    {
        pathID = in.readInt();
        super.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException
    {
        out.writeInt(pathID);
        super.write(out);
    }

    @Override
    public String toString()
    {
        String output = "";
        for (String str : toStrings())
        {
            output += str + " ";
        }
        return output.substring(0, output.length() - 1);
    }

    @Override
    public FeatureSlotWritable[] get()
    {
        ArrayList<FeatureSlotWritable> output = new ArrayList<>();
        for (Writable writable : super.get())
            output.add((FeatureSlotWritable)writable);

        return output.toArray(new FeatureSlotWritable[output.size()]);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
}
