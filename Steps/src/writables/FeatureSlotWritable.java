package writables;

import org.apache.hadoop.io.WritableComparable;
import slots.FeatureSlot;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class FeatureSlotWritable extends FeatureSlot implements WritableComparable<FeatureSlotWritable>
{
    private FeatureSlotWritable()
    {

    }

    public FeatureSlotWritable(String word, String slotTag, String tfidf, String dice, String mi)
    {
        this(word, slotTag, Double.parseDouble(tfidf), Double.parseDouble(dice), Double.parseDouble(mi));
    }

    public FeatureSlotWritable(String word, String slotTag, double tfidf, double dice, double mi)
    {
        super(word, slotTag, tfidf, dice, mi);
    }

    @Override
    public int compareTo(FeatureSlotWritable other)
    {
        return super.compareTo(other);
    }

    @Override
    public void write(DataOutput out) throws IOException
    {
        out.writeUTF(word);
        out.writeUTF(tag.name());
        out.writeDouble(tfidf);
        out.writeDouble(dice);
        out.writeDouble(mi);
    }

    @Override
    public void readFields(DataInput in) throws IOException
    {
        word = in.readUTF();
        tag = Tag.valueOf(in.readUTF());
        tfidf = in.readDouble();
        dice = in.readDouble();
        mi = in.readDouble();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new FeatureSlotWritable(word, tag.toString(), tfidf, dice, mi);
    }
}