package syntactic;

import slots.FeatureSlot;
import java.text.DecimalFormat;
import java.util.ArrayList;

public final class Measures
{
    private Measures()
    { }

    public static double cover(Triples pathFeatures1, Triples pathFeatures2)
    {
        double coverSlotsX = cover(pathFeatures1.getSlotsX(), pathFeatures2.getSlotsX());
        double coverSlotsY = cover(pathFeatures1.getSlotsY(), pathFeatures2.getSlotsY());

        return formatNumber(Math.sqrt(coverSlotsX * coverSlotsY));
    }

    private static double cover(ArrayList<FeatureSlot> slots1, ArrayList<FeatureSlot> slots2)
    {
        double coverIntersection = 0;
        for (FeatureSlot featureSlot : intersection(slots1, slots2))
        {
            int featureIndex1 = slots1.indexOf(featureSlot);
            coverIntersection += slots1.get(featureIndex1).getDice();
        }

        double cover1 = 0;
        for (FeatureSlot featureSlot : slots1)
        {
            cover1 += featureSlot.getDice();
        }

        return coverIntersection / cover1;
    }

    public static double cosine(Triples pathFeatures1, Triples pathFeatures2)
    {
        double cosineSlotsX = cosine(pathFeatures1.getSlotsX(), pathFeatures2.getSlotsX());
        double cosineSlotsY = cosine(pathFeatures1.getSlotsY(), pathFeatures2.getSlotsY());

        return formatNumber(Math.sqrt(cosineSlotsX * cosineSlotsY));
    }

    private static double cosine(ArrayList<FeatureSlot> slots1, ArrayList<FeatureSlot> slots2)
    {
        double cosineIntersection = 0;
        for (FeatureSlot featureSlot : intersection(slots1, slots2))
        {
            int featureIndex1 = slots1.indexOf(featureSlot);
            int featureIndex2 = slots2.indexOf(featureSlot);
            cosineIntersection += (slots1.get(featureIndex1).getTfidf() * slots2.get(featureIndex2).getTfidf());
        }

        double cosine1 = 0;
        for (FeatureSlot featureSlot : slots1)
        {
            cosine1 += Math.pow(featureSlot.getTfidf(), 2);
        }

        double cosine2 = 0;
        for (FeatureSlot featureSlot : slots2)
        {
            cosine2 += Math.pow(featureSlot.getTfidf(), 2);
        }

        return cosineIntersection / (Math.sqrt(cosine1) * Math.sqrt(cosine2));
    }

    public static double sim(Triples pathFeatures1, Triples pathFeatures2)
    {
        double simSlotsX = sim(pathFeatures1.getSlotsX(), pathFeatures2.getSlotsX());
        double simSlotsY = sim(pathFeatures1.getSlotsY(), pathFeatures2.getSlotsY());

        return formatNumber(Math.sqrt(simSlotsX * simSlotsY));
    }

    private static double sim(ArrayList<FeatureSlot> slots1, ArrayList<FeatureSlot> slots2)
    {
        double miIntersection = 0;
        for (FeatureSlot featureSlot : intersection(slots1, slots2))
        {
            int featureIndex1 = slots1.indexOf(featureSlot);
            int featureIndex2 = slots2.indexOf(featureSlot);
            miIntersection += slots1.get(featureIndex1).getMi() + slots2.get(featureIndex2).getMi();
        }

        double mi1 = 0;
        for (FeatureSlot featureSlot : slots1)
        {
            mi1 += featureSlot.getMi();
        }

        double mi2 = 0;
        for (FeatureSlot featureSlot : slots2)
        {
            mi2 += featureSlot.getMi();
        }

        return miIntersection / (mi1 + mi2);
    }

    public static double mi(double PATH_SLOW_WORD, double STAR_SLOW_STAR, double PATH_SLOT_STAR, double STAR_SLOT_WORD)
    {
        return formatNumber(Math.log((PATH_SLOW_WORD * STAR_SLOW_STAR) / (PATH_SLOT_STAR * STAR_SLOT_WORD)));
    }

    public static double dice(double PATH_SLOW_WORD, double PATH_SLOT_STAR, double STAR_SLOW_WORD)
    {
        return formatNumber((2 * PATH_SLOW_WORD) / (PATH_SLOT_STAR + STAR_SLOW_WORD));
    }

    public static double tfidf(double PATH_SLOW_WORD, double STAR_SLOW_WORD)
    {
        return formatNumber(PATH_SLOW_WORD / (1 + Math.log(STAR_SLOW_WORD)));
    }

    private static double formatNumber(double number)
    {
        try
        {
            // take only 6 decimal digits
            DecimalFormat decimalFormat = new DecimalFormat("##.000000");
            String strNumber = decimalFormat.format(number);

            return Double.parseDouble(strNumber);
        }
        catch (Exception e)
        {
            return number;
        }
    }

    private static <T> ArrayList<T> intersection(ArrayList<T> slots1, ArrayList<T> slots2)
    {
        ArrayList<T> intersection = new ArrayList<>(slots1);
        intersection.retainAll(slots2);
        return intersection;
    }
}
