package syntactic;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import slots.Slot.SlotX;
import slots.Slot.SlotY;

import java.util.*;

public class PathsGenerator
{
    private String[] auxiliarySet = new String[]
            {
                    "do", "does", "did", "has", "have", "had", "is", "am", "are", "was", "were", "be", "being",
                    "been", "may", "must", "might", "should", "could", "would", "shall", "will", "can"
            };
    private final ArrayList<SyntacticToken> syntacticArray;
    private final HashSet<String> auxiliaryVerbs;
    private int counter;
    private int rootPlace;

    public PathsGenerator()
    {
        this.syntacticArray = new ArrayList<>();
        this.counter = 0;
        this.rootPlace = 0;
        this.auxiliaryVerbs = new HashSet<>();
        Collections.addAll(auxiliaryVerbs, auxiliarySet);
    }

    public void clear()
    {
        this.counter = 0;
        this.rootPlace = 0;
        this.syntacticArray.clear();
    }

    public void insert(SyntacticToken token)
    {
        syntacticArray.add(token);
        if (token.isRoot())
            rootPlace = counter;

        counter++;
    }

    public LinkedList<SyntacticPath> generatePaths()
    {
        LinkedList<SyntacticPath> pathsList = new LinkedList<>();

        // check if the root is a verb
        SyntacticToken root = syntacticArray.get(rootPlace);
        if (!root.isVerb() || auxiliaryVerbs.contains(root.getWord()))
            return pathsList;

        // find first noun to be in slotX
        for (int i = 0; i < syntacticArray.size(); i++)
        {
            // skip the root word
            if (i == rootPlace)
                continue;

            SyntacticToken tokenX = syntacticArray.get(i);
            if (tokenX.isNoun())
            {
                // create SlotX and create pathX for tokenX
                SlotX slotX = new SlotX(tokenX.getWord());
                HashSet<Integer> indexPath = createIndexPathFromToken(tokenX, i);
                String pathX = createPathFromToken("X", tokenX, i, new HashSet<Integer>());

                // find second noun to be in slotY
                for (int j = i + 1; j < syntacticArray.size(); j++)
                {
                    if (j == rootPlace || indexPath.contains(j))
                    {
                        continue;
                    }

                    SyntacticToken tokenY = syntacticArray.get(j);
                    if (tokenY.isNoun())
                    {
                        // create slotY and create pathY for tokenY
                        SlotY slotY = new SlotY(tokenY.getWord());
                        String pathY = createPathFromToken("Y", tokenY, j, indexPath);

                        // combine both paths from X and Y to the root
                        String path = pathX + " " + root.getWord() + " " + pathY;
                        // add the path and the slots to paths map
                        pathsList.add(new SyntacticPath(path, slotX, slotY));

                        // reverse the paths
                        String reversedPath = path.replace("X", "XX").replace("Y", "X").replace("XX", "Y");
                        // add the path and the slots to paths map -- reversed!
                        pathsList.add(new SyntacticPath(reversedPath, slotX, slotY));

                        // add another reversed path.
                        String reversedPath2 = pathY + " " + root.getWord() + " " + pathX;
                        pathsList.add(new SyntacticPath(reversedPath2, slotX, slotY));
                    }
                }
            }
        }

        return pathsList;
    }

    private HashSet<Integer> createIndexPathFromToken(SyntacticToken token, int tokenPlace)
    {
        HashSet<Integer> indexPath = new HashSet<>();

        do
        {
            indexPath.add(tokenPlace);
            tokenPlace = token.getHeadIndex();
            token = syntacticArray.get(tokenPlace);
        } while (!token.isRoot());

        return indexPath;
    }

    private String createPathFromToken(String slot, SyntacticToken token, int tokenPlace, HashSet<Integer> indexPath)
    {
        String path = slot;

        int nextTokenPlace = token.getHeadIndex();
        SyntacticToken nextToken = syntacticArray.get(token.getHeadIndex());
        // we will end the while when the nextToken will be the root.
        while (!nextToken.isRoot())
        {
            if (!indexPath.contains(nextTokenPlace))
            {
                // if the to tokens are content words, then the dependency relations is included,
                // so we add the nextToken word to the path.
                if (token.isContent() && nextToken.isContent())
                {
                    if (tokenPlace < nextTokenPlace)
                        path += " " + nextToken.getWord();
                    else
                        path = nextToken.getWord() + " " + path;
                }
            }

            tokenPlace = nextTokenPlace;
            nextTokenPlace = nextToken.getHeadIndex();
            token = nextToken;
            nextToken = syntacticArray.get(nextToken.getHeadIndex());
        }

        return path;
    }

}
