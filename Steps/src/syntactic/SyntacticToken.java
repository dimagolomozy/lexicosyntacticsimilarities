package syntactic;

/**
 * Class for a Syntactic token from a syntactic-ngram line
 * Contains:
 *          word        - The word field can contain any non-whitespace character.
 *          posTag      - Is a Penn-Treebank part-of-speech tag.
 *          depLable    - Is a stanford-basic-dependencies label.
 *          headIndex   - Is an integer, pointing to the head of the current token.
 */
public class SyntacticToken
{
    /**
     * Set of all Penn-Treebank Part-Of-Speech tags.
     */
    private enum PosTag
    {
        CC,     // conjunction, coordinating	and, or, but
        CD,     // cardinal number	five, three, 13%
        DT,	    // determiner	the, a, these
        EX,	    // existential there	there were six boys
        FW,	    // foreign word	mais
        IN,	    // conjunction, subordinating or preposition	of, on, before, unless
        JJ,	    // adjective	nice, easy
        JJR,	// adjective, comparative	nicer, easier
        JJS,	// adjective, superlative	nicest, easiest
        LS,	    // list item marker
        MD,	    // verb, modal auxillary	may, should
        NN,	    // noun, singular or mass	tiger, chair, laughter
        NNS,	// noun, plural	tigers, chairs, insects
        NNP,	// noun, proper singular	Germany, God, Alice
        NNPS,	// noun, proper plural	we met two Christmases ago
        PDT,	// predeterminer	both his children
        POS,	// possessive ending	's
        PRP,	// pronoun, personal	me, you, it
        PRP$,	// pronoun, possessive	my, your, our
        RB,	    // adverb	extremely, loudly, hard
        RBR,	// adverb, comparative	better
        RBS,	// adverb, superlative	best
        RP,	    // adverb, particle	about, off, up
        SYM,	// symbol	%
        TO,	    // infinitival to	what to do?
        UH,	    // interjection	oh, oops, gosh
        VB,	    // verb, base form	think
        VBZ,	// verb, 3rd person singular present	she thinks
        VBP,	// verb, non-3rd person singular present	I think
        VBD,	// verb, past tense	they thought
        VBN,	// verb, past participle	a sunken ship
        VBG,	// verb, gerund or present participle	thinking is fun
        WDT,	// wh-determiner	which, whatever, whichever
        WP,	    // wh-pronoun, personal	what, who, whom
        WP$,	// wh-pronoun, possessive	whose, whosever
        WRB;    // wh-adverb	where, when

        private static boolean isVerb(PosTag tag)
        {
            return (tag == VB) || (tag == VBZ) || (tag == VBP) || (tag == VBD)
                    || (tag == VBN) || (tag == VBG) || (tag == MD);
        }

        private static boolean isNoun(PosTag tag)
        {
            return (tag == NN) || (tag == NNS) || (tag == NNP) || (tag == NNPS);
        }

        private static boolean isAdjective(PosTag tag)
        {
            return (tag == JJ) || (tag == JJR) || (tag == JJS);
        }

        private static boolean isAdverb(PosTag tag)
        {
            return (tag == RB) | (tag == RBR) || (tag == RBS) || (tag == RP);
        }

        private static boolean isIn(PosTag tag)
        {
            return (tag == IN);
        }

        private static boolean isTo(PosTag tag)
        {
            return (tag == TO);
        }

        private static boolean isContent(PosTag tag)
        {
            return isNoun(tag) || isVerb(tag) || isAdjective(tag) || isAdverb(tag) || isIn(tag) || isTo(tag);
        }
    }

    private final String word;
    private final PosTag posTag;
    private final int headIndex;

    public SyntacticToken(String word, String posTag, String headIndex) throws Exception
    {
        this.word = word;
        this.posTag = PosTag.valueOf(posTag.toUpperCase());
        this.headIndex = Integer.parseInt(headIndex) - 1;   // we start counting from 0!
    }

    public int getHeadIndex()
    {
        return headIndex;
    }

    public PosTag getPosTag()
    {
        return posTag;
    }

    public String getWord()
    {
        return word;
    }

    public boolean isNoun()
    {
        return PosTag.isNoun(this.posTag);
    }

    public boolean isRoot()
    {
        return (headIndex == -1);
    }

    public boolean isContent()
    {
        return PosTag.isContent(this.posTag);
    }

    public boolean isVerb()
    {
        return PosTag.isVerb(this.posTag);
    }

}
