package syntactic;

import slots.FeatureSlot;

import java.util.ArrayList;

public class Triples
{
    private final String path;
    private final ArrayList<FeatureSlot> slotsX;
    private final ArrayList<FeatureSlot> slotsY;

    public Triples(String path)
    {
        this.path = path;
        this.slotsX = new ArrayList<>();
        this.slotsY = new ArrayList<>();
    }

    public void insert(FeatureSlot featureSlot)
    {
        if (featureSlot.isSlotX())
            slotsX.add(featureSlot);
        else
            slotsY.add(featureSlot);
    }

    public ArrayList<FeatureSlot> getSlotsX()
    {
        return slotsX;
    }

    public ArrayList<FeatureSlot> getSlotsY()
    {
        return slotsY;
    }

    public boolean isEmpty()
    {
        return slotsX.isEmpty() || slotsY.isEmpty();
    }

    @Override
    public String toString()
    {
        return path + " " + slotsX.toString() + " " + slotsY.toString();
    }
}