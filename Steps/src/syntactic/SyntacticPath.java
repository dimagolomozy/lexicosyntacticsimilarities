package syntactic;

import slots.Slot;

public class SyntacticPath
{
    private final String path;
    private final Slot slotX;
    private final Slot slotY;

    public SyntacticPath(String path, Slot slotX, Slot slotY)
    {
        this.path = path;
        this.slotX = slotX;
        this.slotY = slotY;
    }

    public Slot getSlotX()
    {
        return slotX;
    }

    public Slot getSlotY()
    {
        return slotY;
    }

    public String getPath()
    {
        return path;
    }
}
