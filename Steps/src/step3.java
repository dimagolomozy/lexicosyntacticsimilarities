import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import syntactic.Measures;
import writables.FeatureSlotArrayWritable;
import writables.FeatureSlotWritable;
import writables.SyntacticSlotWritable;

import java.io.IOException;
import java.util.ArrayList;

public class step3
{
    private final static String READABLE_OUTPUT = "readable";
    private final static String USE_READABLE_OUTPUT = "use.readable.output";
    private final static String SORTER_STRING = "" + (char)31;

    /**
     * Input:
     *      Output of Step 2
     *  Format:
     *      path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
     *
     * Output:
     *      Send to reducer the same line. but when writing to context, the mapper count
     *      the STAR_SLOTX_STAR and STAR_SLOTY_STAR and send its in the end of the work to all the reducers
     *      with (char)31 to calculate the c(*, slot, *)
     *
     *  Format:
     *      SORTER_STRING TAB Fake_SyntacticSlotWritable
     *      path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
     *
     */
    private static class myMapper extends Mapper<Text, SyntacticSlotWritable, Text, SyntacticSlotWritable>
    {
        private static long STAR_SLOTX_STAR = 0;
        private static long STAR_SLOTY_STAR = 0;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            super.setup(context);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException
        {
            // get the number of the reducers
            int numPartitions = ((JobConf)context.getConfiguration()).getNumReduceTasks();

            // create value to pass
            SyntacticSlotWritable value = new SyntacticSlotWritable("", "", STAR_SLOTX_STAR, STAR_SLOTY_STAR, 0);
            // create key
            Text key = new Text(SORTER_STRING);

            // write to all the reducers. The myPartitioner sends to all the reducers
            for (int i = 0; i < numPartitions; ++i)
            {
                context.write(key, value);
            }

            super.cleanup(context);
        }

        @Override
        public void map(Text path, SyntacticSlotWritable syntacticSlotWritable, Context context)
                throws IOException,  InterruptedException
        {
            // insert : path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
            context.write(path, syntacticSlotWritable);

            // let the mapper count that, and then send to reducer only 1 line from each mapper!
            if (syntacticSlotWritable.isSlotX())
                STAR_SLOTX_STAR += syntacticSlotWritable.getPATH_SLOT_WORD();
            if (syntacticSlotWritable.isSlotY())
                STAR_SLOTY_STAR += syntacticSlotWritable.getPATH_SLOT_WORD();
        }
    }

    /**
     * Splits the input with more reducers.
     * Based on the hashCode of the path.
     * And if the key is SORTER_STRING then returns all the reducers numbers one by one.
     */
    private static class myPartitioner extends Partitioner<Text, SyntacticSlotWritable>
    {
        private static int num = 0;

        @Override
        public int getPartition(Text key, SyntacticSlotWritable value, int numPartitions)
        {
            String strKey = key.toString();
            if (strKey.equals(SORTER_STRING))
            {   // will enter here numPartitiones time. and will send back every time num++.
                // this will cause the mapper to send to each reducer a line
                int old = num++;
                if (num == numPartitions) num = 0;
                return old;
            }

            return Math.abs(strKey.hashCode()) % numPartitions;
        }
    }

    /**
     * Input:
     *      The mapper output.
     *  Format:
     *      SORTER_STRING TAB Fake_SyntacticSlotWritable
     *      path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD
     *
     * Output:
     *      Calculates all the measures for all the slots in a given path.
     *      Outputs a line of path with all the slots (slotX or slotY) with SPACE separated
     *  Format:
     *      path TAB word1/slotTag/tfidf/dice/mi SPACE word2/slotTag/tfidf/dice/mi SPACE.....
     */
    private static class myReducer extends Reducer<Text, SyntacticSlotWritable, Text, FeatureSlotArrayWritable>
    {
        private static long STAR_SLOTX_STAR = 0;
        private static long STAR_SLOTY_STAR = 0;

        private static MultipleOutputs MOS;
        private static boolean USE_MOS;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            super.setup(context);
            MOS = new MultipleOutputs<>(context);

            USE_MOS = context.getConfiguration().getBoolean(USE_READABLE_OUTPUT, false);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException
        {
            MOS.close();
            super.cleanup(context);
        }

        @Override
        public void reduce(Text key, Iterable<SyntacticSlotWritable> values, Context context)
                throws IOException,  InterruptedException
        {
            String strKey = key.toString();

            if (strKey.equals(SORTER_STRING))
            {   // mean that this is the fake syntacticSlot. SLOTX is in PATH_SLOW_WORD. SLOTY is in PATH_SLOT_STAR

                // calculate STAR_SLOTX_STAR and STAR_SLOTY_STAR
                STAR_SLOTX_STAR = 0;
                STAR_SLOTY_STAR = 0;
                for (SyntacticSlotWritable syntacticSlotWritable : values)
                {
                    STAR_SLOTX_STAR += syntacticSlotWritable.getPATH_SLOT_WORD();
                    STAR_SLOTY_STAR += syntacticSlotWritable.getPATH_SLOT_STAR();
                }
            }
            else
            {   // means: path TAB word1/slotTag/PATH_SLOT_WORD/PATH_SLOT_STAR/STAR_SLOT_WORD

                ArrayList<FeatureSlotWritable> featureArray = new ArrayList<>();
                for (SyntacticSlotWritable syntacticSlotWritable : values)
                {
                    double PATH_SLOT_WORD = (double)syntacticSlotWritable.getPATH_SLOT_WORD();
                    double STAR_SLOT_WORD = (double)syntacticSlotWritable.getSTAR_SLOT_WORD();
                    double PATH_SLOT_STAR = (double)syntacticSlotWritable.getPATH_SLOT_STAR();

                    double tfidf = 0, dice = 0, mi = 0;
                    if (syntacticSlotWritable.isSlotX())
                    {
                        tfidf = Measures.tfidf(PATH_SLOT_WORD, STAR_SLOT_WORD);
                        dice = Measures.dice(PATH_SLOT_WORD, PATH_SLOT_STAR, STAR_SLOT_WORD);
                        mi = Measures.mi(PATH_SLOT_WORD, STAR_SLOTX_STAR, PATH_SLOT_STAR, STAR_SLOT_WORD);
                    }
                    if (syntacticSlotWritable.isSlotY())
                    {
                        tfidf = Measures.tfidf(PATH_SLOT_WORD, STAR_SLOT_WORD);
                        dice = Measures.dice(PATH_SLOT_WORD, PATH_SLOT_STAR, STAR_SLOT_WORD);
                        mi = Measures.mi(PATH_SLOT_WORD, STAR_SLOTY_STAR, PATH_SLOT_STAR, STAR_SLOT_WORD);
                    }

                    // create FeatureSlotWritable and add it to the featureArray
                    FeatureSlotWritable featureSlotWritable = new
                            FeatureSlotWritable(syntacticSlotWritable.getWord(), syntacticSlotWritable.getSlotTag(), tfidf, dice, mi);
                    featureArray.add(featureSlotWritable);
                }

                // create new key
                Text path = new Text(strKey);
                // create new value
                FeatureSlotArrayWritable featureSlotArrayWritable =
                        new FeatureSlotArrayWritable(featureArray.toArray(new FeatureSlotWritable[featureArray.size()]));

                // write it to seq file
                context.write(path, featureSlotArrayWritable);

                // write it to readable file
                if (USE_MOS)
                    MOS.write(READABLE_OUTPUT, path, featureSlotArrayWritable, "_"  + READABLE_OUTPUT);
            }
        }
    }

    /**
     * Main of Step 3 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - Reducers amount
     *            args[3] - use Readable
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        System.out.println("# Init Step 3");
        Configuration conf = new Configuration();
        conf.setBoolean(USE_READABLE_OUTPUT, Boolean.parseBoolean(args[3]));

        Job job = new Job(conf, "Step 3");
        job.setNumReduceTasks(Integer.parseInt(args[2]));
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(SyntacticSlotWritable.class);
        job.setJarByClass(step3.class);
        job.setMapperClass(step3.myMapper.class);
        job.setPartitionerClass(step3.myPartitioner.class);
        job.setReducerClass(step3.myReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FeatureSlotArrayWritable.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        MultipleOutputs.addNamedOutput(job, READABLE_OUTPUT, TextOutputFormat.class, Text.class, Text.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.out.println("# Starting Step 3");
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }


}
